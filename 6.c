#include <stdio.h>
#include <string.h>
#include <wchar.h>
#include <stdlib.h>
#include <locale.h>
#define SEAT_NUM 12

// структура для представления
// одного места в самолете
typedef struct _MinskAirlinesSeat
{
	int ID;
	short isTaken;
	char firstName[41];
	char lastName[41];
dsa
} MinskAirlinesSeat;

int numOfFreeSeats();dsfsda
void showListOfFreeSeats();
void showListOfTakenSeats();
void bookASeat();
void unbookASeat();
void unloadAndExit();
void initialize();

// указатель на массив мест в самолете
MinskAirlinesSeat **park;
char *fileName = "MinskAirlinesInfo";

int main()
{
    setlocale(LC_ALL, "ru_RUS.utf-8");
    
    // меню отображает функционал,
    // заложенный в систему
	char *menu[] = {
		"Показать количество свободных мест",
		"Показать список свободных мест",
		"Показать список забронированных мест в алфавитном порядке",
		"Забронировать место для пассажира",
		"Снять бронь с места",
		"Выйти из программы"
	};
	int n = 6;
	int i;
	int choice;
	initialize();

	do
	{
		for (i = 0; i < n; i++)
			printf("%d) %s\n", i + 1, menu[i]);
		printf("Ваш выбор: ");

        scanf("%d", &choice);
		printf("\n");

		switch (choice)
		{
		case 1:
		{
					printf("Количество свободных мест: %d\n", numOfFreeSeats());
					break;
		}
		case 2:
		{
					showListOfFreeSeats();
					break;
		}
		case 3:
		{
					showListOfTakenSeats();
					break;
		}
		case 4:
		{
					bookASeat();
					getchar();
					break;
		}
		case 5:
		{
					unbookASeat();
					getchar();
					break;
		}
		case 6:
		{
					unloadAndExit();
					return 0;
		}
		default:
		{
				   printf("Неизвестная операция: %d.\n", choice);
		}
		}

		printf("\n");

	} while (1);

		return 0;
}

void initialize()
{
	park = (MinskAirlinesSeat**)calloc(SEAT_NUM, sizeof(MinskAirlinesSeat*));
	FILE *info = fopen( fileName, "rb");

	if (info != NULL)
	{
		MinskAirlinesSeat temp;
		int i = 0;
		while ( i < 12 && fread(&temp, sizeof(MinskAirlinesSeat), 1, info))
		{
			park[temp.ID - 1] = calloc(1, sizeof(MinskAirlinesSeat));
			park[temp.ID - 1]->ID = temp.ID;
			park[temp.ID - 1]->isTaken = 1;
            strcpy(park[temp.ID - 1]->firstName, temp.firstName);
			strcpy(park[temp.ID - 1]->lastName, temp.lastName);
			i++;
		}

		fclose(info);
	}

}

int numOfFreeSeats()
{
	int free = SEAT_NUM;
	int i;
	for (i = 0; i < SEAT_NUM; i++)
	{
		if (park[i] != NULL && park[i]->isTaken)
			free--;
	}

	return free;
}

void showListOfFreeSeats()
{
	int i;
	printf("Свободные места:\n");
	for (i = 0; i < SEAT_NUM; i++)
	{
		if (park[i] == NULL || !park[i]->isTaken)
		{
			printf("%d\n", i + 1);
		}

	}
}
int alphabeticOrderComparer(const void *a, const void *b)
{
    MinskAirlinesSeat *A = (MinskAirlinesSeat*)a;
    MinskAirlinesSeat *B = (MinskAirlinesSeat*)b;

    int res = strcmp(A->lastName, B->lastName);
    return res != 0 ? res : strcmp(A->firstName, B->firstName);
}

void showListOfTakenSeats()
{
    int taken = SEAT_NUM - numOfFreeSeats();
    int i, j;
    MinskAirlinesSeat **takenSeats;

	if (taken == 0)
	{
		printf("Все места свободны.\n");
		return;
	}

    takenSeats = (MinskAirlinesSeat**)calloc(taken, sizeof(MinskAirlinesSeat*));
    for(i = 0, j = 0; i < SEAT_NUM; i++)
    {
        if(park[i] != NULL && park[i]->isTaken)
            takenSeats[j++] = park[i];
    }

    qsort(takenSeats, taken, sizeof(MinskAirlinesSeat*), alphabeticOrderComparer);

	printf("Забронированные места:\n\n");
	for (i = 0; i < taken; i++)
	{
			printf("Место №%d\n", takenSeats[i]->ID);
			printf("Фамилия: %s\n", takenSeats[i]->lastName);
			printf("Имя: %s\n\n", takenSeats[i]->firstName);
	}
}

void bookASeat()
{
	int seat;
	printf("Вводите номер желаемого места или -1 для выхода в меню.\n");
	printf("Номер места для бронирования: ");

	do
	{
		scanf("%d", &seat);

		if (seat == -1)
			break;
		if (seat < 1 || seat > 12)
		{
			printf("Неверный ввод! Повторите попытку: ");
			continue;
		}
		seat--;

		if (park[seat] == NULL)
		{
			park[seat] = calloc(1, sizeof(MinskAirlinesSeat));
			park[seat]->ID = seat + 1;
		}

		if (!park[seat]->isTaken)
		{
			char fName[41], lName[41];
			int sure;

			printf("Фамилия: ");
			scanf("%40s", lName);
			printf("Имя: ");
			scanf("%40s", fName);

			printf("Подтвердите бронирование (1 - да, иначе - выход в меню): ");
			scanf("%d", &sure);
			if (sure)
			{
				strcpy(park[seat]->firstName, fName);
				strcpy(park[seat]->lastName, lName);
				park[seat]->isTaken = 1;
				printf("Место успешно забронированно!\n");
			}
			else
			{
				free(park[seat]);
				park[seat] = NULL;
			}
			break;
		}
		else
		{
			printf("Место уже занято! Выберите другое: ");
		}

	} while (1);


}

void unbookASeat()
{
	int seat;
    printf("Вводите номер забронированного места или -1 для выхода в меню.\n");
    printf("Номер места для снятия брони: ");

	do
	{
		scanf("%d", &seat);

		if (seat == -1)
			break;
		if (seat < 1 || seat > 12)
		{
			printf("Неверный ввод! Повторите попытку: ");
			continue;
		}
		seat--;

		if (park[seat] == NULL || !park[seat]->isTaken)
		{
			printf("Ошибка! Место свободно. Выберите другое: ");
			continue;
		}
		else
		{
            int sure;
            printf("Подтвердите снятие брони (1 - да, иначе - выход в меню): ");
            scanf("%d", &sure);

            if(sure)
            {
                park[seat]->isTaken = 0;
                printf("Бронь успешно снята!\n");
            }
			break;
		}

	} while (1);


}

// завершающая функция
// информация о забронированных местах
// записывается в файл
void unloadAndExit()
{
	FILE *info = fopen(fileName, "wb");
	int i = 0;

	if (info != NULL)
	{
		for (; i < SEAT_NUM; i++)
		{
			if (park[i] != NULL)
			{
                if(park[i]->isTaken)
                    fwrite(park[i], sizeof(MinskAirlinesSeat), 1, info);

				free(park[i]);
			}
		}
		fclose(info);
	}

	free(park);
}
